package hska.project.ccnn;

import java.util.Vector;

import org.neuroph.core.data.DataSetRow;

/**
 * 
 * @author Kevin Dietrich
 *
 */
public class Neuron {
	
	/**
	 * Represents the <b>index</b> of the neuron in an layer of the network.
	 */
	private int _index;
	private int _layer;
	private double _outputValue;
	private double _gradient;
	private double _gradientAscent;
	private Vector<Double> _residualError;
	private Vector<Connection> _inputWeights;
	private final static double ETA = 5.0;
	
	public Neuron(int index, int layer) {
		setInputWeights(new Vector<Connection>());
		_index = index;
		_layer = layer;
		setResidualError(new Vector<>());
	}
	
	public void feedForward(Vector<Neuron> previousNeurons) {
//		System.out.println("-#- Feedforward -#-");
		double sum = 0.0;
		for (int n = 0; n < _inputWeights.size(); n++) {
			//Output values of previous neurons must be sorted like this:
			//[I0, I1, ... In, H0, H0, ... Hn]
			//I represents the input neurons and H the hidden neurons but
			//always the first neuron of an hidden layer because cascade-correlation structure
			//And the weights must be sorted the same as the neurons
			sum += previousNeurons.get(n).getOutputValue() * _inputWeights.get(n).getWeight();
//			System.out.println("Sum: " + sum + " = Prev Output: "
//							   + previousNeurons.get(n).getOutputValue()
//							   + " * Weight: " + _inputWeights.get(n).getWeight());
		}
		_outputValue = transferFunction(sum);
//		System.out.println("Output: " + _outputValue + " = f(" + sum + ")");
	}
	
	public void feedForward2(Vector<Double> inputValues) {
		double sum = 0.0;
		for (int w = 0; w < _inputWeights.size(); w++) {
			sum += inputValues.get(w) * _inputWeights.get(w).getWeight();
		}
		_outputValue = transferFunction(sum);
	}
	
	public void calcOutputGradients(double targetValue) {
//		System.out.println("-#- Gradient calculation -#-");
		double delta = _outputValue - targetValue;
//		System.out.println("Delta: " + delta + " = Output: " + _outputValue
//						   + " - Target: " + targetValue);
		_gradient = delta * transferFunctionDerivative(_outputValue);
//		System.out.println("Gradient: " + _gradient + " = Delta " + delta + " * f'(" 
//						   + _outputValue + ")");
		getResidualError().add(delta);
	}
	
	public void updateDeltaWeights(Vector<Neuron> previous) {
//		System.out.println("-#- DeltaWeights calculation -#-");
		for (int i = 0; i < _inputWeights.size(); i++) {
			Connection connection = _inputWeights.get(i);
			double updatedWeight = - ETA * (_gradient * previous.get(i).getOutputValue());
//			System.out.println("UpdatedWeight: " + updatedWeight + " = Weight: "
//							   + connection.getWeight() + " - 0,5 * (Gradient: "
//							   + _gradient + " * Output: " + previous.get(i).getOutputValue() + ")");
			double newDeltaWeight = connection.getDeltaWeight() + updatedWeight;
//			System.out.println("NewDeltaWeight: " + newDeltaWeight + " = DeltaWeight: "
//								+ connection.getDeltaWeight() + " + UpdatedWeight: "
//								+ updatedWeight);
			connection.setDeltaWeight(newDeltaWeight);
		}
	}

	public void updateWeightsWithBatch(int sampleDataSize) {
		System.out.println("-#- Update Weights -#-");
		for (int i = 0; i < _inputWeights.size(); i++) {
			Connection connection = _inputWeights.get(i);
			double newWeight = connection.getWeight() + connection.getDeltaWeight() / sampleDataSize;
			System.out.println("NewWeight: " + newWeight + " = Weight: "
								+ connection.getWeight() + " + DeltaWeight: "
								+ connection.getDeltaWeight());
			connection.setWeight(newWeight);
			connection.setDeltaWeight(0.0);
		}
	}
	
	public void calculateCorrChain(int sampleDataSize, DataSetRow values) {
		System.out.println("Correlation Chain");
	}
	
	public void updateCandidateDeltaWeights(Vector<Double> prevOutputValues, double calcError, double candidateOutput) {
		for (int i = 0; i < _inputWeights.size(); i++) {
			Connection connection = _inputWeights.get(i);
			double gradient = calcError * transferFunctionDerivative(candidateOutput) * prevOutputValues.get(i);
			double updatedWeight = - ETA * gradient;
			double newDeltaWeight = connection.getDeltaWeight() + updatedWeight;
			connection.setDeltaWeight(newDeltaWeight);
		}
	}
	
	public void updateCandidateWeightsWithBatch(int sampleDataSize) {
		for (int i = 0; i < _inputWeights.size(); i++) {
			Connection connection = _inputWeights.get(i);
			double newWeight = connection.getWeight() + connection.getDeltaWeight();
			connection.setWeight(newWeight);
			connection.setDeltaWeight(0.0);
		}
	}
	
	public void clearResidualErrors() {
		getResidualError().clear();
	}
	
	private double transferFunction(double x) {
		return (1.0 / (1.0 + Math.exp(-x)));
	}
	
	private double transferFunctionDerivative(double x) {
		return x * (1.0 - x);
	}
	
	public void setOutputValue(double value) {
		_outputValue = value;
	}
	
	public double getOutputValue() {
		return _outputValue;
	}
	
	public int getIndex() {
		return _index;
	}
	
	public void setLayer(int layer) {
		_layer = layer;
	}
	
	public int getLayer() {
		return _layer;
	}

	public Vector<Connection> getInputWeights() {
		return _inputWeights;
	}

	public void setInputWeights(Vector<Connection> _inputWeights) {
		this._inputWeights = _inputWeights;
	}

	public Vector<Double> getResidualError() {
		return _residualError;
	}

	public void setResidualError(Vector<Double> _residualError) {
		this._residualError = _residualError;
	}

}
