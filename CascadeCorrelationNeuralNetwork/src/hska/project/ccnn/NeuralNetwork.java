package hska.project.ccnn;

import java.util.Vector;

import org.neuroph.core.data.DataSetRow;

/**
 * 
 * @author Kevin Dietrich
 *
 */
public class NeuralNetwork {
	
	private Vector<DataSetRow> _trainingsDataSet;
	private Vector<Vector<Double>> _inputValueSetExtended;
	private Vector<Vector<Double>> _targetValueSet;
	private int _inputValuesSize;
	private int _sampleDataSize;
	private int _hiddenNeuronCount = 0;
	private Vector<Double> _candidateOutputValues;
	private Vector<Vector<Double>> _errorsOfOutputLayer;
	
	/**
	 * Holds the neural network layers with each of its neurons.
	 */
	private Vector<Vector<Neuron>> _layers;
	private double _totalError;
	private double _sumOfSquaredError = Double.MAX_VALUE;
	private double _standardDeviation = 0.0;
	private double _correlation = 0.0;

	/**
	 * Constructor - For creation of the neural network based on the topology.
	 * @param topology
	 */
	public NeuralNetwork(int[] topology, Vector<DataSetRow> trainingsDataSet) {
		_trainingsDataSet = trainingsDataSet;
		_sampleDataSize = trainingsDataSet.size();
		_inputValueSetExtended = new Vector<>();
		_targetValueSet = new Vector<>();
		_candidateOutputValues = new Vector<>();
		_errorsOfOutputLayer = new Vector<>();
		createNetwork(topology);
		createExtendedInputValueSet(trainingsDataSet);
		createTargetValueSet(trainingsDataSet);
	}
	
	private void createExtendedInputValueSet(Vector<DataSetRow> trainingsDataSet) {
		_inputValuesSize = trainingsDataSet.firstElement().getInput().length;
		for (DataSetRow row : trainingsDataSet) {
			Vector<Double> inputValues = new Vector<>();
			for (int i = 0; i < row.getInput().length; i++) {
				inputValues.add(row.getInput()[i]);
			}
			//Add bias output value
			inputValues.add(1.0);
			_inputValueSetExtended.add(inputValues);
		}
//		System.out.println(_inputValueSetExtended.size());
	}
	
	private void createTargetValueSet(Vector<DataSetRow> trainingsDataSet) {
		for (DataSetRow row : trainingsDataSet) {
			Vector<Double> targetValues = new Vector<>();
			for (int i = 0; i < row.getDesiredOutput().length; i++) {
				targetValues.add(row.getDesiredOutput()[i]);
			}
			_targetValueSet.add(targetValues);
		}
	}
	
	/**
	 * 
	 * @param topology
	 */
	private void createNetwork(int[] topology) {
		_layers = new Vector<>();
		
		//Add input layer
		_layers.add(new Vector<Neuron>());
		for (int n = 0; n < topology[0] + 1; n++) {
			Neuron neuron = new Neuron(n, 0);
			_layers.lastElement().add(neuron);
		}
		//Set the output of the bias neuron to 1.0
		_layers.firstElement().lastElement().setOutputValue(1.0);
		
		//Add output layer
		_layers.add(new Vector<Neuron>());
		for (int n = 0; n < topology[1]; n++) {
			Neuron neuron = new Neuron(n, 1);
			for (int i = 0; i < topology[0] + 1; i++) {
				neuron.getInputWeights().add(new Connection());
			}
			_layers.lastElement().add(neuron);				
		}
	}
	
	public void printNetwork() {
		System.out.println("Layers: " + _layers.size());
		
		//Print input neurons
		Vector<Neuron> layer = _layers.firstElement();
		System.out.println("Neuronen: " + layer.size());
		for (Neuron n : layer) {
			System.out.println("Neuron: " + n.getIndex() + " in Layer: " + n.getLayer());
		}
		
		//Print hidden neurons
		if (_layers.size() > 2) {
			for (int l = 1; l < _layers.size() - 1; l++) {
				System.out.println("Neuronen: " + _layers.get(l).size());
				//Input neurons always must be added to previous layer
				Vector<Neuron> previousNeurons = new Vector<>();
				previousNeurons.addAll(_layers.firstElement());
				
				//Only trigger when two hidden layers exists
				for (int h = 2; h <= l; h++) {
					Neuron neuron = _layers.get(h - 1).firstElement();
					previousNeurons.add(neuron);
				}
				Neuron hiddenNeuron = _layers.get(l).firstElement();
				System.out.println("Neuron: " + hiddenNeuron.getIndex()
									+ " in Layer: " + hiddenNeuron.getLayer());
				
				for (Neuron prevNeuron : previousNeurons) {
					System.out.println("Connection von Neuron: " + prevNeuron.getIndex()
										+ " in Layer: " + prevNeuron.getLayer());
				}
			}
		}
		
		//Print output neurons
		System.out.println("Neuronen: " + _layers.lastElement().size());
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			//Input neurons always must be added to previous layer
			Vector<Neuron> previousNeurons = new Vector<>();
			previousNeurons.addAll(_layers.firstElement());
			//Get all layers between the input layer and the output layer
			for (int l = 1; l < _layers.size() - 1; l++) {
				//Only add the first neuron of the layer because
				//Cascade-Correlation don't allow more than one neuron in hidden layers
				Neuron neuron = _layers.get(l).firstElement();
				previousNeurons.add(neuron);
			}
			Neuron outputNeuron = _layers.lastElement().get(o);
			System.out.println("Neuron: " + outputNeuron.getIndex()
								+ " in Layer: " + outputNeuron.getLayer());
			
			for (Neuron prevNeuron : previousNeurons) {
				System.out.println("Connection von Neuron: " + prevNeuron.getIndex()
									+ " in Layer: " + prevNeuron.getLayer());
			}			
		}
	}
	
	/**
	 * 
	 * @param inputValues
	 */
	public void feedForward(DataSetRow inputValues) {		
		//Feedforward input neurons
		for (int i = 0; i < inputValues.getInput().length; i++) {
			_layers.firstElement().get(i).setOutputValue(inputValues.getInput()[i]);
		}
		
		//Feedforward hidden neurons
		for (int l = 1; l < _layers.size() - 1; l++) {
			//Input neurons always must be added to previous layer
			Vector<Neuron> previousNeurons = new Vector<>();
			previousNeurons.addAll(_layers.firstElement());
			
			//Only trigger when two hidden layers exists
			if (l > 1) {
				for (int h = 2; h <= l; h++) {
					Neuron neuron = _layers.get(h - 1).firstElement();
					previousNeurons.add(neuron);
				}
			}
			_layers.get(l).firstElement().feedForward(previousNeurons);
		}
		
		//Feedforward output neurons
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			//Input neurons always must be added to previous layer
			Vector<Neuron> previousNeurons = new Vector<>();
			previousNeurons.addAll(_layers.firstElement());
			//Get all layers between the input layer and the output layer
			for (int l = 1; l < _layers.size() - 1; l++) {
				//Only add the first neuron of the layer because
				//Cascade-Correlation don't allow more than one neuron in hidden layers
				Neuron neuron = _layers.get(l).firstElement();
				previousNeurons.add(neuron);
			}
			_layers.lastElement().get(o).feedForward(previousNeurons);
		}
	}
	
	public void calcNetworkError(DataSetRow targetValues) {
		Vector<Neuron> outputLayer = _layers.lastElement();
		double error = 0.0;
		Vector<Double> errors = new Vector<>();
		
		for (int n = 0; n < outputLayer.size(); ++n) {
			double delta = targetValues.getDesiredOutput()[n] - outputLayer.get(n).getOutputValue();
			error += 0.5 * (delta * delta);
			errors.add(0.5 * (delta * delta));
		}
		_sumOfSquaredError += error;
		
		double averageSSE = _sumOfSquaredError /_sampleDataSize;
		for (Double value : errors) {
			_standardDeviation += Math.pow(value - averageSSE, 2);
		}
		_standardDeviation += Math.sqrt(_standardDeviation);
	}
	
	public void updateDeltaWeights(DataSetRow targetValues) {
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			//Input neurons always must be added to previous layer
			Vector<Neuron> previousNeurons = new Vector<>();
			previousNeurons.addAll(_layers.firstElement());
			//Get all layers between the input layer and the output layer
			for (int l = 1; l < _layers.size() - 1; l++) {
				//Only add the first neuron of the layer because
				//Cascade-Correlation don't allow more than one neuron in hidden layers
				Neuron neuron = _layers.get(l).firstElement();
				previousNeurons.add(neuron);
			}
			_layers.lastElement().get(o).calcOutputGradients(targetValues.getDesiredOutput()[o]);
			_layers.lastElement().get(o).updateDeltaWeights(previousNeurons);
		}
	}
	
	public void updateWeightsWithBatch() {
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			_layers.lastElement().get(o).updateWeightsWithBatch(_sampleDataSize);
		}
	}
	
	public void calculateCorrChain(DataSetRow values) {
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			_layers.lastElement().get(o).calculateCorrChain(_sampleDataSize, values);
		}
	}
	
	public void recruitCandidate() {
		Neuron candidate = new Neuron(0, ++_hiddenNeuronCount);
		for (int l = 0; l < _hiddenNeuronCount; l++) {
			for (int n = 0; n < _layers.get(l).size(); n++) {
				candidate.getInputWeights().add(new Connection());
			}
		}
		Vector<Neuron> hiddenLayer = new Vector<>();
		hiddenLayer.add(candidate);
		_layers.add(_layers.size() - 1, hiddenLayer);
		
		//Update output layer
		for (int n = 0; n < _layers.lastElement().size(); n++) {
			Neuron neuron = _layers.lastElement().get(n);
			neuron.setLayer(neuron.getLayer() + 1);
//			neuron.getInputWeights().add(new Connection());
		}
		_candidateOutputValues.clear();
	}
	
	public void trainCandidate() {
		_candidateOutputValues.clear();
		Neuron candidate = _layers.get(_hiddenNeuronCount).get(0);
		
//		//Get previous neurons
//		Vector<Neuron> previousNeurons = new Vector<>();
//		previousNeurons.addAll(_layers.firstElement());
//		for (int l = 1; l < candidate.getLayer(); l++) {
//			Neuron neuron = _layers.get(l).firstElement();
//			previousNeurons.add(neuron);
//		}
		
		//Feedforward candidate neuron
		for (int i = 0; i < _inputValueSetExtended.size(); i++) {
			candidate.feedForward2(_inputValueSetExtended.get(i));
			_candidateOutputValues.add(candidate.getOutputValue());
		}
		
		double corr = 0.0;
		double norm = 0.0;
		
		//Calculate correlation and updateWeights
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			Vector<Double> residualErrors = _layers.lastElement().get(o).getResidualError();
			double sum = 0.0;
			double averageError = 0.0;
			double averageOutput = 0.0;
			double var1 = 0.0;
			double var2 = 0.0;
			
			for (int e = 0; e < residualErrors.size(); e++) {
				averageError += residualErrors.get(e);
			}
			averageError /= residualErrors.size();
			
			for (int v = 0; v < _candidateOutputValues.size(); v++) {
				averageOutput += _candidateOutputValues.get(v);
			}
			averageOutput /= _candidateOutputValues.size();
			
			for (int i = 0; i < _inputValueSetExtended.size(); i++) {
				double error = residualErrors.get(i);
				sum += (_candidateOutputValues.get(i) - averageOutput) * (error - averageError);
				var1 += Math.pow(_candidateOutputValues.get(i) - averageOutput, 2);
				var2 += Math.pow(error - averageError, 2);
				candidate.updateCandidateDeltaWeights(_inputValueSetExtended.get(i), (error - averageError), _candidateOutputValues.get(i));
			}
			corr += Math.abs(sum);
			norm += Math.sqrt(var1 * var2);
		}
		_correlation += (corr / norm);
	}
	
	public void updateCandidateWeights() {
		_layers.get(_hiddenNeuronCount).firstElement().updateCandidateWeightsWithBatch(_sampleDataSize);
	}
	
	public void addCandidateValuesToExtendedValues() {
		for (int i = 0; i < _inputValueSetExtended.size(); i++) {
			_inputValueSetExtended.get(i).add(_candidateOutputValues.get(i));
		}
	}
	
	public void activateCandidate() {
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			Neuron output = _layers.lastElement().get(o);
			output.getInputWeights().add(new Connection());
		}
	}
	
	public void clearResidualErrors() {
		for (int o = 0; o < _layers.lastElement().size(); o++) {
			_layers.lastElement().get(o).clearResidualErrors();
		}
	}
	
	/**
	 * 
	 * @param resultValues
	 */
	public Vector<Double> getResults() {
		Vector<Double> outputValues = new Vector<>();
		for (int n = 0; n < _layers.lastElement().size(); ++n) {
			outputValues.addElement(_layers.lastElement().get(n).getOutputValue());
		}
		return outputValues;
	}

	public double getTotalError() {
		return _totalError;
	}

	public double getSumOfSquaredError() {
		return _sumOfSquaredError;
	}
	
	public void setSumOfSquaredError(double value) {
		_sumOfSquaredError = value;
	}

	public double getCorrelation() {
		return _correlation;
	}

	public void setCorrelation(double _correlation) {
		this._correlation = _correlation;
	}

	public double getStandardDeviation() {
		return _standardDeviation;
	}

	public void setStandardDeviation(double _standardDeviation) {
		this._standardDeviation = _standardDeviation;
	}
}
