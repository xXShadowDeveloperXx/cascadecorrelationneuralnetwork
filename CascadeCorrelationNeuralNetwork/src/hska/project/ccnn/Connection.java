package hska.project.ccnn;

import java.util.Random;

/**
 * Die Klasse stellt eine Verbindung zwischen zwei Neuronen dar.
 * 
 * @author Kevin Dietrich
 *
 */
public class Connection {

	private double _weight;
	
	private double _deltaWeight;
	
	public Connection() {
		_weight = randomWeight();
		_deltaWeight = 0.0;
	}
	
	private double randomWeight() {
		Random rand = new Random();
		return rand.nextDouble();
	}

	public double getWeight() {
		return _weight;
	}
	
	public double getDeltaWeight() {
		return _deltaWeight;
	}
	
	public void setWeight(double weight) {
		_weight = weight;
	}
	
	public void setDeltaWeight(double deltaWeight) {
		_deltaWeight = deltaWeight;
	}

}
