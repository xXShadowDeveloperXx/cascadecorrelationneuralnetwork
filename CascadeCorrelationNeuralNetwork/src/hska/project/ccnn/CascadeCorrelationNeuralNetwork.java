package hska.project.ccnn;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;

import com.opencsv.CSVReader;

public class CascadeCorrelationNeuralNetwork {
	
	private final int TRAININGSDATA_PERCENTAGE = 60;
	
	private int _epoch = 1;
	private double _userError = 0.0;
	private int _maxEpochs;
	private NeuralNetwork _neuralNetwork;
	
	private Vector<DataSetRow> _trainingsSet;
	private Vector<DataSetRow> _validationSet;
	private Vector<DataSetRow> _testSet;
	
	public CascadeCorrelationNeuralNetwork(int[] topology,
			double userError,
			int maxEpochs,
			DataSet trainingsDataSet) {
		
		_userError = userError;
		_maxEpochs = maxEpochs;
		_trainingsSet = new Vector<>();
		_validationSet = new Vector<>();
		_testSet = new Vector<>();
		prepareDataSets(trainingsDataSet);

		_trainingsSet = prepareDataSet();
		_neuralNetwork = new NeuralNetwork(topology, prepareDataSet());
		_neuralNetwork.printNetwork();
	}
	
	private Vector<DataSetRow> prepareDataSet() {
		Vector<DataSetRow> dataSet = new Vector<>();
		
		DataSetRow row = new DataSetRow(new double[]{0, 0}, new double[]{0});
		dataSet.add(row);
		row = new DataSetRow(new double[]{0, 1}, new double[]{1});
		dataSet.add(row);
		row = new DataSetRow(new double[]{1, 0}, new double[]{1});
		dataSet.add(row);
		row = new DataSetRow(new double[]{1, 1}, new double[]{0});
		dataSet.add(row);
		
		return dataSet;
	}
	
	private void prepareDataSets(DataSet trainingsDataSet) {
		int trainingsDataCount = (trainingsDataSet.size() * TRAININGSDATA_PERCENTAGE) / 100;
		int validationDataCount = (trainingsDataSet.size() - trainingsDataCount) / 2;
		int testDataCount = trainingsDataSet.size() - trainingsDataCount - validationDataCount;
		int outputCount = 2;
		
//		System.out.println(trainingsDataSet.size());
//		System.out.println(trainingsDataCount);
//		System.out.println(validationDataCount);
//		System.out.println(testDataCount);
		
		//Fill data sets with the first half of the whole training set
		fillSets(trainingsDataSet, _trainingsSet, (trainingsDataCount / outputCount));
		fillSets(trainingsDataSet, _validationSet, (validationDataCount / outputCount));
		fillSets(trainingsDataSet, _testSet, (testDataCount / outputCount));
		
		//Fill data sets with the second half of the whole training set
		fillSets(trainingsDataSet, _trainingsSet, (trainingsDataCount / 2));
		fillSets(trainingsDataSet, _validationSet, (validationDataCount / 2));
		fillSets(trainingsDataSet, _testSet, (testDataCount / 2));
		
		//Print actual Training set
//		for (DataSetRow data : _trainingsSet) {
//			for (double i : data.getInput()) {
//				System.out.print(i + " ");
//			}
//			for (double r : data.getDesiredOutput()) {
//				System.out.print(r + " ");
//			}
//			System.out.println();
//		}
		
//		System.out.println(_trainingsSet.size());
//		System.out.println(_validationSet.size());
//		System.out.println(_testSet.size());
		
		long seed = System.nanoTime();
		Collections.shuffle(_trainingsSet, new Random(seed));
		Collections.shuffle(_validationSet, new Random(seed));
		Collections.shuffle(_testSet, new Random(seed));
	}
	
	private void fillSets(DataSet trainingsDataSet,
						  Vector<DataSetRow> inputSet,
						  int dataSetSize) {
		
		assert(trainingsDataSet.size() >= dataSetSize);
		
		for (int i = 0; i < dataSetSize; i++) {
			inputSet.add(trainingsDataSet.getRowAt(i));
		}
		for (int i = 0; i < dataSetSize; i++) {
			trainingsDataSet.removeRowAt(0);
		}
	}
	
	public void train() {
		boolean isTraining = true;
		boolean firstRun = false;
		int epoch = 1;
		double oldError = 1.0;
		double oldStandardDev = Double.MAX_VALUE;
		double lamda = 0.000001;
		while (isTraining) {
//			if (((epoch != _maxEpochs) && (_userError < _neuralNetwork.getSumOfSquaredError())
//				&& ((oldError - lamda) >= _neuralNetwork.getSumOfSquaredError())) || firstRun) {
//
//				oldError = _neuralNetwork.getSumOfSquaredError();
//				trainingPhase();
//				epoch++;
//				firstRun = false;
//			} else if (_userError >= _neuralNetwork.getSumOfSquaredError()) {
//				System.out.println("Training finished!");
//				isTraining = false;				
//			} else {
//				_neuralNetwork.recruitCandidate();
//				System.out.println("Recruit Candidate!");
////				_neuralNetwork.printNetwork();
//				inputPhase();
//				_neuralNetwork.activateCandidate();
//				epoch = 1;
//				oldError = Double.MAX_VALUE;
//				_neuralNetwork.setSumOfSquaredError(Double.MAX_VALUE);
//				//output phase
//			}
			
			if ((_userError >= _neuralNetwork.getStandardDeviation()) && firstRun) {
				System.out.println("Training finished!");
				isTraining = false;
				firstRun = true;
			} else if ((epoch == _maxEpochs) || ((oldStandardDev - lamda) <= _neuralNetwork.getStandardDeviation())) {
				_neuralNetwork.recruitCandidate();
				System.out.println("Recruit Candidate!");
				inputPhase();
				_neuralNetwork.activateCandidate();
				epoch = 1;
				oldError = Double.MAX_VALUE;
			} else {
				trainingPhase();
				oldError = _neuralNetwork.getStandardDeviation();
				epoch++;
			}
		}
	}
	
	private void inputPhase() {
		boolean isTraining = true;
		boolean firstRun = false;
		int epoch = 1;
		double oldCorrelation = 0.0;
		double lamda = 0.00001;
		while (isTraining) {
//			if (((epoch != _maxEpochs) && ((oldCorrelation + lamda) < _neuralNetwork.getCorrelation())) || firstRun) {
//				oldCorrelation = _neuralNetwork.getCorrelation();
//				_neuralNetwork.setCorrelation(0.0);
//				_neuralNetwork.trainCandidate();
//				_neuralNetwork.updateCandidateWeights();
//				epoch++;
//				firstRun = false;
////			} else if (epoch == _maxEpochs) {
////				isTraining = false;
////				_neuralNetwork.addCandidateValuesToExtendedValues();
//			} else {
//				isTraining = false;
//				_neuralNetwork.setCorrelation(0.0);
//				_neuralNetwork.addCandidateValuesToExtendedValues();
//			}
			
			if (((epoch == _maxEpochs) || ((oldCorrelation + lamda) > _neuralNetwork.getCorrelation()))
					&& firstRun) {
				isTraining = false;
				_neuralNetwork.setCorrelation(0.0);
				_neuralNetwork.addCandidateValuesToExtendedValues();
			} else {
				oldCorrelation = _neuralNetwork.getCorrelation();
				_neuralNetwork.setCorrelation(0.0);
				_neuralNetwork.trainCandidate();
				_neuralNetwork.updateCandidateWeights();
				epoch++;
				firstRun = true;
			}
		}
	}
	
	private boolean trainingPhase() {
		System.out.println("##### " + _epoch++ + ". Epoch #####");
		_neuralNetwork.clearResidualErrors();
		_neuralNetwork.setSumOfSquaredError(0.0);
		_neuralNetwork.setStandardDeviation(0.0);
		for (int i = 0; i < _trainingsSet.size(); i++) {
			double[] input = _trainingsSet.get(i).getInput();
//			System.out.print("Input: ");
//			for (int j = 0; j < input.length; j++) {
//				System.out.print(input[j] + " ");
//			}
//			System.out.println();
			_neuralNetwork.feedForward(_trainingsSet.get(i));
//			System.out.print("Output: ");
//			for (Double output : _neuralNetwork.getResults()) {
//				System.out.print(output + " ");
//			}
//			System.out.println();
			double[] target = _trainingsSet.get(i).getDesiredOutput();
//			System.out.print("Target: ");
//			for (int j = 0; j < target.length; j++) {
//				System.out.print(target[j] + " ");
//			}
//			System.out.println();
			_neuralNetwork.calcNetworkError(_trainingsSet.get(i));
			_neuralNetwork.updateDeltaWeights(_trainingsSet.get(i));
//			_neuralNetwork.calculateCorrChain(_trainingsSet.get(i));
		}
		_neuralNetwork.updateWeightsWithBatch();
		System.out.println("Sum of squared Error: " + _neuralNetwork.getSumOfSquaredError());
		System.out.println("Standard Deviation: " + _neuralNetwork.getStandardDeviation());
		return true;
	}

	public static void main(String[] args) {
		String fileName = "res/iris_c3.csv";		
		
		//TrainingsData
		DataSet trainingSet = new DataSet(4, 1);
		
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(fileName));

			String[] header = reader.readNext();
//			System.out.println(header[0]);
	
			String [] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				for (String s : nextLine) {				
					double[] input = new double[4];
					double[] result = new double[1];
					int index = 0;
					for (int i = 0; i < s.length(); i++) {
						if (!(s.indexOf('"') == i)) {
							if (s.charAt(i) == ',') {
								for (int j = i + 1; j < s.length(); j++) {
									if (s.charAt(j) == ',') {
										String inputValue = s.substring(i + 1, j);
										input[index++] = Double.valueOf(inputValue);
										break;
									}
								}
							}
						} else {
							break;
						}
					}
					if (s.contains("setosa")) {
						result[0] = 1.0;
					} else if (s.contains("virginica")) {
						result[0] = 0.0;
					}
					trainingSet.addRow(new DataSetRow(input, result));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Topology of the Neural Network
		int[] topology = {2,1};
		
		//Create a new Neural Network
		CascadeCorrelationNeuralNetwork ccnn = new CascadeCorrelationNeuralNetwork(topology,
																				   0.001,
																				   1500,
																				   trainingSet);
		ccnn.train();
	}
}